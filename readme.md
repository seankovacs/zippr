# Zippr
### Dependencies
Need Go installed. [Download Here](https://golang.org/)

### Build 
`go build main.go server.go`

### Run
1. Start the server:
    ##### OSX 
    `./main`
    ##### WIN 
    `main.exe`

2. Couple of options to test:
    * Use the built in test site to POST data: http://localhost/index.html
    * Use curl/other to POST to the endpoint: http://localhost/zip
        > curl -vX POST -d @/path/to/file/sample_archive.json 'http://localhost/zip' --header "Content-Type: application/json" --output test.zip

### Tests
`go test`

