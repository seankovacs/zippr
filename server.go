package main

import (
	"archive/zip"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// ZipItem - zip item model
type ZipItem struct {
	URL      string `json:"url"`
	Filename string `json:"filename"`
}

// newServer returns  a new server instance
func newServer(port string, readTimeout time.Duration, writeTimeout time.Duration) *http.Server {
	return &http.Server{
		Addr:         port,
		Handler:      nil,
		ReadTimeout:  readTimeout * time.Second,
		WriteTimeout: writeTimeout * time.Second,
	}
}

// responseHandler handles incoming requests.
func responseHandler(w http.ResponseWriter, req *http.Request) {
	// fmt.Printf("Incoming request at path %v, method %v\n", req.URL.Path, req.Method)
	if req.Method != "POST" {
		http.Error(w, "Bad request.", http.StatusBadRequest)
		return
	}

	// Parse JSON body
	var data []ZipItem
	err := json.NewDecoder(req.Body).Decode(&data)
	if err != nil {
		http.Error(w, "Bad request."+err.Error(), http.StatusBadRequest)
		return
	}

	// Ensure client supports streaming data back
	ctx := req.Context()
	flusher, ok := w.(http.Flusher)
	if !ok {
		http.Error(w, "Streaming unsupported!", http.StatusBadRequest)
		return
	}

	// Set headers required for streaming, add dispo to enable file download
	w.Header().Set("Content-Disposition", "attachment; filename=archive.zip")
	w.Header().Set("Content-Type", "application/zip")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	// Send 200, letting browser know we're still here
	w.WriteHeader(http.StatusOK)

	// Setup zip writer - will stream out to res writer
	archiver := zip.NewWriter(w)

	// Deferred Closer for archive
	defer archiver.Close()

	// Since the bottle neck I believe will be the client <-> server, I think this is fine.
	// This can be sped up by adding in some concurrency - multiple http request,
	// with the downloaded files going into a queue waiting to be pumped out to the client via the zip writer.
	// Realistically though, if we're talking > 500MB, it really should be an async request. Click "I want my data" -> "Processing...check email",
	// kick off a job, email the person with a link afterwards. Expire the file after 72 hours.

	for _, zip := range data {
		select {
		case <-ctx.Done(): // End if client closes connection
			fmt.Println("Client stopped listening.")
			return
		default:
			processFile(archiver, zip.URL, zip.Filename)
			flusher.Flush()
		}
	}
}

// processFile will take an zip Writer pointer, download URL, and file name
// It will attempt to download and insert the item into the archive
func processFile(archiver *zip.Writer, downloadURL string, filename string) error {
	// Download file from URL
	res, err := http.Get(downloadURL)
	if err != nil {
		return fmt.Errorf("failed downloading file: %v", filename)
	}

	// Read body into byte array
	defer res.Body.Close()
	file, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("failed reading file: %v", filename)
	}

	// Add file to archive
	f, err := archiver.Create(filename)
	if err != nil {
		return fmt.Errorf("failed creating zip item for file: %v", filename)
	}

	_, err = f.Write(file)
	if err != nil {
		return fmt.Errorf("failed writing to zip for file: %v", filename)
	}

	return nil
}
