package main

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestResponseHandler(t *testing.T) {
	// Load test file
	jsonFile, err := os.Open("./sample_archive_small.json")
	if err != nil {
		t.Fatal(err)
	}
	defer jsonFile.Close()

	req, err := http.NewRequest("POST", "/zip", jsonFile)
	req.Header.Add("Content-Type", "application/json;charset=utf-8")
	req.Header.Add("Connection", "keep-alive")
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	http.HandlerFunc(responseHandler).ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		fmt.Println(rr.Body.String())
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expectedContentType := "application/zip"
	if rr.Header().Get("Content-Type") != expectedContentType {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Header().Get("Content-Type"), expectedContentType)
	}
}

func TestProcessFile(t *testing.T) {

	// Create zip file
	buf := new(bytes.Buffer)
	w := zip.NewWriter(buf)
	err := processFile(w, "https://d3dgy5wiit1ici.cloudfront.net/photos/db8a1f5d-af54-4157-a7d3-4e12e90fb167.jpg", "test.jpg")
	if err != nil {
		t.Errorf("unexpected error in processFile: %v", err.Error())
	}
	w.Close()

	ioutil.WriteFile("test.zip", buf.Bytes(), 0666)

	// Read zip and verify file
	r, err := zip.OpenReader("test.zip")
	if err != nil {
		t.Errorf("unexpected error opening zip: %v", err.Error())
	}

	testFile := r.File[0]
	if testFile.Name != "test.jpg" {
		t.Errorf("expected zip to contain test.jpg, got %v", testFile.Name)
	}
	r.Close()

	// cleanup
	os.Remove("test.zip")
}
