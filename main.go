package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	// Create server instance on port 80, 30 sec read timeout, 600 sec write,; set primary handler
	http.Handle("/", http.FileServer(http.Dir("./static")))
	http.HandleFunc("/zip", responseHandler)
	server := newServer(":80", 30, 600)
	fmt.Printf("Listining to request on port %v\n", server.Addr)
	log.Fatal(server.ListenAndServe())
}
